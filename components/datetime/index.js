// pages/components/datetime/index.js
const date = new Date();
Component({
	properties: {
		//返回格式  1、2019-09-09 12:00:00  2、2019/09/09 21:00:00 3、2019年01月01日 00:00:00 4、
		showFormat: {
			type: String,
			value: 1
		},
		tips: {
			type: String,
			value: '请选择时间'
		},
		//选择设置选中项、展示项
		multiIndex: {
			type: Array,
			value: [0, 0, 0, 0, 0, 0],
		},
		//年份起止 [起始年份,最大年份] 默认当前年
		yearRange: {
			type: Array,
			value: [],
		},
		//显示格式 1所有带汉字  2 日期带汉字   3 只显示数字
		isChinese: {
			type: Number,
			value: 2,
		},
	},
	data: {
		time: '',
		tipsBlock: false,
		// multiArray: [years, months, days, hours, minutes],
		// multiIndex: [0, 9, 16, 10, 17],
		choose_year: '',
	},

	methods: {
		//格式整理
		format: function(e) {
			return e.replace(/[^0-9]/ig, "");
		},
		//返回格式整理
		returnFormat: function(format, index, lastTime) {
			for (let i = 0; i < this.data.multiIndex.length; i++) {
				if (lastTime == undefined || lastTime == null || lastTime == '') {
					lastTime = this.format(this.data.multiArray[i][index[i]])
				} else {
					if (i == 3) {
						lastTime = lastTime + " " + this.format(this.data.multiArray[i][index[i]])
					} else if (i > 3) {
						lastTime = lastTime + ":" + this.format(this.data.multiArray[i][index[i]])
					} else {
						lastTime = lastTime + format + this.format(this.data.multiArray[i][index[i]])
					}
				}
			}
			return lastTime
		},
		//获取开始时间日期
		bindMultiPickerChange: function(e) {
			this.setData({
				multiIndex: e.detail.value
			})
			const index = this.data.multiIndex;
			var lastTime = '';
			switch (Number(this.data.showFormat)) {
				case 1:
					lastTime = this.returnFormat('-', index, lastTime)
					break;
				case 2:
					lastTime = this.returnFormat('/', index, lastTime)
					break;
				case 3:
					for (let i = 0; i < this.data.multiIndex.length; i++) {
						if (i == 3) {
							lastTime = lastTime + " " + this.format(this.data.multiArray[i][index[i]])
						} else if (i >= 3) {
							lastTime = lastTime + ":" + this.format(this.data.multiArray[i][index[i]])
						} else {
							lastTime = lastTime + this.data.multiArray[i][index[i]]
						}
					}
					break;
				case 4:
					lastTime = this.returnFormat('~', index, lastTime)
					break;
				case 5:
					lastTime = this.returnFormat('*', index, lastTime)
					break;
				default:
					for (let i = 0; i < this.data.multiIndex.length; i++) {
						if (i == 3) {
							lastTime = lastTime + " " + this.data.multiArray[i][index[i]]
						} else {
							lastTime = lastTime + this.data.multiArray[i][index[i]]
						}
					}
					break;
			}

			this.setData({
				time: lastTime,
				tipsBlock: true
			})
		},
		//监听picker的滚动事件
		bindMultiPickerColumnChange: function(e) {
			//获取年份
			if (e.detail.column == 0) {
				let choose_year = this.data.multiArray[e.detail.column][e.detail.value];
				this.setData({
					choose_year
				})
			}
			if (e.detail.column == 1) {
				let num = parseInt(this.data.multiArray[e.detail.column][e.detail.value]);
				let temp = [];
				if (num == 1 || num == 3 || num == 5 || num == 7 || num == 8 || num == 10 || num == 12) { //判断31天的月份
					for (let i = 1; i <= 31; i++) {
						if (i < 10) {
							i = "0" + i;
						}
						if (Number(this.data.isChinese) == 3) {
							temp.push("" + i);
						} else {
							temp.push("" + i + "日");
						}

					}
					this.setData({
						['multiArray[2]']: temp
					});
				} else if (num == 4 || num == 6 || num == 9 || num == 11) { //判断30天的月份
					for (let i = 1; i <= 30; i++) {
						if (i < 10) {
							i = "0" + i;
						}
						if (Number(this.data.isChinese) == 3) {
							temp.push("" + i);
						} else {
							temp.push("" + i + "日");
						}
					}
					this.setData({
						['multiArray[2]']: temp
					});
				} else if (num == 2) { //判断2月份天数
					let year = parseInt(this.data.choose_year);
					console.log(year);
					if (((year % 400 == 0) || (year % 100 != 0)) && (year % 4 == 0)) {
						for (let i = 1; i <= 29; i++) {
							if (i < 10) {
								i = "0" + i;
							}
							if (Number(this.data.isChinese) == 3) {
								temp.push("" + i);
							} else {
								temp.push("" + i + "日");
							}
						}
						this.setData({
							['multiArray[2]']: temp
						});
					} else {
						for (let i = 1; i <= 28; i++) {
							if (i < 10) {
								i = "0" + i;
							}
							if (Number(this.data.isChinese) == 3) {
								temp.push("" + i);
							} else {
								temp.push("" + i + "日");
							}
						}
						this.setData({
							['multiArray[2]']: temp
						});
					}
				}
			}
			var data = {
				multiArray: this.data.multiArray,
				multiIndex: this.data.multiIndex
			};
			data.multiIndex[e.detail.column] = e.detail.value;
			this.setData(data);
		},
		getYear:function(){
			const years = []
			if (this.data.yearRange.length > 0) {
				if (this.data.yearRange.length == 1) {
					//获取年
					for (let i = this.data.yearRange[0]; i <= date.getFullYear() + 5; i++) {
						if (Number(this.data.isChinese)== 3) {
							years.push("" + i);
						} else {
							years.push("" + i + "年");
						}
					}
				} else {
					//获取年
					for (let i = this.data.yearRange[0]; i <= this.data.yearRange[1]; i++) {
						if (Number(this.data.isChinese) == 3) {
							years.push("" + i);
						} else {
							years.push("" + i + "年");
						}
						console.log(i)
					}
				}
			} else {
				//获取年
				for (let i = date.getFullYear(); i <= date.getFullYear() + 5; i++) {
					if (Number(this.data.isChinese) == 3) {
						years.push("" + i);
					} else {
						years.push("" + i + "年");
					}
				}
			}
			return years
		},
		//获取日期 月 日
		getDate:function(num,isChinese,Chinese){
			const dateNew = [];
			for (let i = 1; i <= num; i++) {
				if (i < 10) {
					i = "0" + i;
				}
				if (Number(this.data.isChinese) == isChinese) {
					dateNew.push("" + i);
				} else {
					dateNew.push("" + i + Chinese);
				}
			}
			return dateNew;
		},
		//获取日期  时 分 秒
		getDates:function(num,isChinese,Chinese){
			const dateNew = [];
			for (let i = 0; i < num; i++) {
				if (i < 10) {
					i = "0" + i;
				}
				if (Number(this.data.isChinese) == isChinese) {
					dateNew.push("" + i + Chinese);
				} else {
					dateNew.push("" + i );
				}
			}
			return dateNew;
		},
	},
	ready: function(e) {
		this.setData({
			multiArray: [this.getYear(), this.getDate(12,3,'月'), this.getDate(31,3,'日'), this.getDates(24,1,'时'), this.getDates(60,1,'分'), this.getDates(60,1,'秒')].slice(0, this.data.multiIndex.length),
		})
	},
})
